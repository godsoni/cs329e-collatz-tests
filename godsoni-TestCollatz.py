#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval_2, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "1 100000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,1)
        self.assertEqual(j,100000)

    def test_read_3(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 1)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval_2(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval_2(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval_2(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval_2(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval_2(10000, 110000)
        self.assertEqual(v, 354)

    def test_eval_6(self):
        v = collatz_eval_2(9500, 19000)
        self.assertEqual(v, 279)

    def test_eval_7(self):
        v = collatz_eval_2(80, 90)
        self.assertEqual(v, 111)

    def test_eval_8(self):
        v = collatz_eval_2(4000, 5000)
        self.assertEqual(v, 215)

    def test_eval_9(self):
        v = collatz_eval_2(60000, 80000)
        self.assertEqual(v, 351)

    def test_eval_10(self):
        v = collatz_eval_2(1, 1000)
        self.assertEqual(v, 179)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 4000, 5000, 215)
        self.assertEqual(w.getvalue(), "4000 5000 215\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 60000, 80000, 351)
        self.assertEqual(w.getvalue(), "60000 80000 351\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage-3.5 run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage-3.5 report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
